The idea behind this is to store Singularity images in a docker like registry but using ORA format.
But we are also showing how to build the singularity image in one of the runners that support priviledge mode, and this is `docker7`. For that we need to add a tag in the CI job.
```
# Build Singularity container
singularity-image:
  # Tags need to be used here in order to run on a runner with priviledge mode enabled
  tags:
    - docker7
  stage: buildImage

```
# Manually building the image
If you are confident building your images localy in your workstation, you can just push the binary file  after manually building it.
`sudo singularity build contai.sif Singularity`

# Manually pushing the file
```
14:18 $ singularity remote add embl https://git.embl.de:4567
INFO:    Remote "embl" added.
Generate an access token at https://git.embl.de:4567/auth/tokens, and paste it here.
Token entered will be hidden for security.
Access Token: 
```

Then creat Registry write access token.

# Running the image
```
singularity run  oras://git.embl.de:4567/moscardo/singularity-registry/singularity-registry:latest cat /etc/os-release
```
